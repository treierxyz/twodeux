# This Python file uses the following encoding: utf-8
import os
from pathlib import Path
import sys
import json

from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtCore import QObject, Slot, Signal
filename = "andmed.json"

data = {"tasks": []}
jsondata = {"tasks": []}

class TaskClass:
    def __init__(self, id, name, time="Täna", done=False):
        self.id = id
        self.name = name
        self.time = time
        self.done = done

    def printNameAndTime(self):
        return self.name + " " + self.time


class MainWindow(QObject):
    def __init__(self):
        QObject.__init__(self)

    loader = Signal(int, str)

    @Slot(int, str)
    def update(self, id, name):
        for task in data["tasks"]:
            if task.id == id:
                if task.name != name:
                    task.name = name
        if data["tasks"] == []:
            data["tasks"].append(TaskClass(id, name))
        elif data["tasks"][-1].id < id:
            data["tasks"].append(TaskClass(id, name))

    @Slot(int)
    def delete(self, id):
        # Kustuta antud indeksiga tegevus
        del data["tasks"][id]
        # Tee ülejäänud tegevuste ID'd uuesti üksteisele järgnevaks
        newId = 0
        for task in data["tasks"]:
            task.id = newId
            newId += 1

    @Slot()
    def salvesta(self):
        # Tühjenda jsondata salvestamise jaoks
        jsondata = {"tasks": []}
        # Lisa kõik tegevused sinna
        for task in data["tasks"]:
            jsondata["tasks"].append(task.__dict__)
        # Salvesta JSON andmed faili
        with open(filename, "w") as file:
            json.dump(jsondata, file, indent=4)

    @Slot()
    def lae(self):
        # Lae faili andmed jsondata muutujasse
        with open(filename, "r") as file:
            jsondata = json.load(file)
        # Teiselda JSON andmed klassi objektideks
        loopId = 0
        for task in jsondata["tasks"]:
            data["tasks"].append(TaskClass(loopId, task["name"], task["time"], task["done"]))
            loopId += 1
        # Saada andmed signaaliga tagasi kasutajaliidesele
        for task in data["tasks"]:
            self.loader.emit(task.id, task.name)


if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()

    # Context
    main = MainWindow()
    engine.rootContext().setContextProperty("backend", main)

    # Lae QML fail
    engine.load(os.fspath(Path(__file__).resolve().parent / "main.qml"))

    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec())
