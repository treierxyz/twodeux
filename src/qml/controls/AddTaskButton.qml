import QtQuick 2.12
import QtQuick.Controls 2.12

RoundButton {
    id: titleButton
    property url iconSource: "../../../assets/ui/add.svg"
    property color colorDefault: "#464646"
    property color colorHover: "#505050"
    property color colorPressed: "#404040"

    QtObject {
        id: internal
        // Hover ja pressed värvid
        property var dynamicColor: if(titleButton.down){
                                       titleButton.down ? colorPressed : colorDefault
                                   } else {
                                       titleButton.hovered ? colorHover : colorDefault
                                   }
    }

    width: 40
    height: titleButton.width
    visible: true
    flat: false
    background: Rectangle {
        id: rectangle
        color: internal.dynamicColor
        radius: titleButton.width/4

        Image {
            id: icon
            source: iconSource
            antialiasing: false
            smooth: true
            x: 0
            y: 0
            width: 26
            height: 26
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:16}D{i:3}
}
##^##*/
