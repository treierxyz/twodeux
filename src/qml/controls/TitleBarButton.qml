import QtQuick 2.12
import QtQuick.Controls 2.12

RoundButton {
    id: titleButton
    property url iconSource: "../../../assets/ui/titlebar_buttons/close.svg"
    property color colorDefault: "#464646"
    property color colorHover: "#505050"
    property color colorPressed: "#404040"

    QtObject {
        id: internal
        // Hover ja pressed värvid
        property var dynamicColor: if(titleButton.down){
                                       titleButton.down ? colorPressed : colorDefault
                                   } else {
                                       titleButton.hovered ? colorHover : colorDefault
                                   }
    }

    width: 22
    height: 22
    visible: true
    radius: 12
    flat: false
    background: Rectangle {
        id: rectangle
        color: internal.dynamicColor
        radius: titleButton.width

        Image {
            id: icon
            source: iconSource
            antialiasing: false
            smooth: true
            x: 0
            y: 0
            width: 12
            height: 12
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:16}D{i:3}
}
##^##*/
