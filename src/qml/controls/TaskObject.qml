import QtQuick 2.12

Item {
    id: taskObject
    width: 860
    height: 40

    property string taskName: ""

    function updateWidth() {
        if (name.text === ""){
            typeSomething.opacity = 1
            name.width = typeSomething.width
        }
        else {
            typeSomething.opacity = 0
            name.width = undefined
            name.width = name.width + 100
        }
    }

    Rectangle {
        id: background
        color: "#3c3c3c"
        radius: taskObject.height/4
        anchors.fill: parent

        MouseArea {
            anchors.fill: parent
            onPressed: {
                parent.forceActiveFocus()
            }
        }

        Text {
            id: typeSomething
            x: 10
            y: 6
            color: "#666666"
            text: "Type something..."
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 17
            anchors.leftMargin: 10
            opacity: 1
        }

        TextInput {
            id: name
            x: 10
            y: 6
            color: "#ffffff"
            text: taskName
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.NoWrap
            persistentSelection: false
            activeFocusOnPress: true
            selectionColor: "#7e9b9b9b"
            selectByMouse: true
            cursorVisible: false
            font.pointSize: 17
            anchors.leftMargin: 10
            onAccepted: {
                mainWindow.salvestaTask(index, this.text)
            }
            onFocusChanged: {
                mainWindow.salvestaTask(index, this.text)
            }
            onTextChanged: {
                taskObject.updateWidth()
            }
            Component.onCompleted: {
                taskObject.updateWidth()
            }
        }
        TitleBarButton {
            id: deleteTask
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
            onReleased: {
                mainWindow.delTask(index)
                listView.listModel.remove(index)
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:1.1}
}
##^##*/
