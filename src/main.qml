import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import "qml/controls"

Window {
    id: mainWindow
    width: 900
    height: 700
    visible: true
    color: "transparent"
    title: "twoDeux"

    // Tiitliriba eemaldamine
    flags: Qt.Window | Qt.FramelessWindowHint

    function salvestaTask(id, name) {
        backend.update(id, name)
        backend.salvesta()
    }

    function laeTasks() {
        backend.lae()
    }

    function delTask(id) {
        backend.delete(id)
        backend.salvesta()
    }

    QtObject {
        id: internal

        function maximizeButton() {
            if (mainWindow.visibility != Window.Maximized){
                mainWindow.showMaximized()
            } else {
                mainWindow.showNormal()
            }
        }
    }

    Rectangle {
        id: bg
        color: "#2E2E2E"
        radius: 20
        anchors.fill: parent
        clip: true

        MouseArea {
            anchors.fill: parent
            onPressed: {
                parent.forceActiveFocus()
            }
        }

        Rectangle {
            id: titlebar
            x: 293
            y: 159
            height: 40
            color: "#353535"
            radius: 20
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            layer.enabled: false
            anchors.rightMargin: 0
            anchors.topMargin: 0
            anchors.leftMargin: 0
            clip: false

            DragHandler {
                onActiveChanged: {
                    if(active) {
                        mainWindow.startSystemMove()
                    }
                }
                target: null
            }

            Rectangle {
                id: titlebar1
                height: titlebar.height/2
                color: parent.color
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: titlebar.height/2
                anchors.rightMargin: 0
                clip: false
                anchors.leftMargin: 0
                layer.enabled: false
            }

            Image {
                id: image
                width: 123
                height: 21
                anchors.left: parent.left
                anchors.top: parent.top
                source: "../assets/logo.png"
                anchors.leftMargin: 13
                anchors.topMargin: 10
                fillMode: Image.PreserveAspectFit
            }
        }
        Row {
            id: titleButtons
            x: 814
            visible: true
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 8
            anchors.topMargin: 9
            spacing: 6
            layoutDirection: Qt.RightToLeft
            topPadding: 0
            rightPadding: 0
            layer.enabled: false
            smooth: true
            clip: false

            TitleBarButton {
                id: roundButtonQuit
                visible: true
                iconSource: "../../../assets/ui/titlebar_buttons/close.svg"
                onClicked: mainWindow.close()
            }
            TitleBarButton {
                id: roundButtonMaximize
                visible: true
                iconSource: "../../../assets/ui/titlebar_buttons/maximize.svg"
                onClicked: internal.maximizeButton()
            }
            TitleBarButton {
                id: roundButtonMinimize
                visible: true
                iconSource: "../../../assets/ui/titlebar_buttons/minimize.svg"
                onClicked: mainWindow.showMinimized()
            }
        }
        ListView {
            id: listView
            anchors.fill: parent
            clip: true
            anchors.rightMargin: 20
            anchors.bottomMargin: 72
            anchors.topMargin: 57
            anchors.leftMargin: 20
            boundsBehavior: Flickable.DragAndOvershootBounds
            spacing: 6
            property ListModel listModel: ListModel {}
            model: listModel
            delegate: TaskObject {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                taskName: name
            }
            ScrollBar.vertical: ScrollBar {
                id: scroll
                parent: listView.parent
                anchors.top: listView.top
                anchors.left: listView.right
                anchors.bottom: listView.bottom
            }
            Component.onCompleted: {
                mainWindow.laeTasks()
            }
        }
        AddTaskButton {
            id: addTaskButton
            x: 833
            y: 641
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.rightMargin: 20
            onReleased: {
                listView.listModel.append({name: ""})
                listView.positionViewAtEnd()
                mainWindow.salvestaTask((listView.listModel.count - 1), "")
            }
        }
    }
    Connections {
        target: backend

        function onLoader(id, name){
            listView.listModel.set(id, {name: name})
        }
    }
}



/*##^##
Designer {
    D{i:0;formeditorZoom:3}D{i:7}
}
##^##*/
