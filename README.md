![](https://gitlab.com/treier57lol/temporaryname/-/raw/main/assets/logo.png)[^1]

---

### Programmeerimise 4. kursuse projekt

Liikmed: Oliver Jõgar, Randal Rainis Kruus, Markus Leivo\
Probleem: Ajaplaneerimine. Võimaldab kasutajal püstitada enda tarbeks ülesandeid ja lisada märkmeid.
Annab kasutajale ülevaate tehtud ja tegemata ülesannetest.

[^1]: Hääldatud nagu "to do" ( IPA: `[tu du:]` ).

Trello: https://trello.com/b/1ZiQISKi

Google Drive projekti dokumendid: https://drive.google.com/drive/folders/13Mcm3LOSB3Xm8zo6lt493p4OZ2xY09ns?usp=sharing

Kasutajavaate prototüübi pildid
![](assets/screenshots/first_design_no_tasks.png)
![](assets/screenshots/first_design_with_tasks.png)

Poster
![](assets/twoDeux.png)
